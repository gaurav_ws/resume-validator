const fs = require('fs');
const debug = require('debug')('resume-validator');

const pdf = require('pdf-parse');
const mammoth = require('mammoth');

const includes = require('includes-validator');

const pdfToBuffer = async filePath => new Promise((resolve, reject) => {
  fs.readFile(filePath, (err, data) => {
    if (err) return reject(err);

    return resolve(data);
  });
});

const getFiles = async dir => new Promise((resolve, reject) => {
  fs.readdir(dir, (err, files) => {
    if (err) return reject(err);

    return resolve(files);
  });
});

const validatePDF = async filePath => pdfToBuffer(filePath)
  .then(pdf)
  .then(({ text }) => (includes.number(text) || includes.email(text)))
  .catch(debug);

const validateDoc = async path => mammoth.extractRawText({ path })
  .then(result => result.value)
  .then(text => (includes.number(text) || includes.email(text)))
  .catch(debug);

const init = async () => {
  const pdfDir = 'pdfs';
  const pdfFiles = await getFiles(pdfDir);

  const docDir = 'docs';
  const docFiles = await getFiles(docDir);

  pdfFiles.map(async (pdfFile) => {
    const isValid = !await validatePDF(`${pdfDir}/${pdfFile}`);
    debug(`${pdfFile} is valid: ${isValid}`);
  });

  docFiles.map(async (docFile) => {
    const isValid = !await validateDoc(`${docDir}/${docFile}`);
    debug(`${docFile} is valid: ${isValid}`);
  });
};


init();
