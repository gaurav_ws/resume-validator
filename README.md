# resume-validator
Helps to Validate resume contents against criteria like having a Phone number, having an Email etc


## Packages used
- `debug`: For logging purposes
- `includes-validator`: For checking if provided text includes email/phone or not. 
- `pdf-parse`: To Convert pdf into to plain text.


## Flow
- Parse PDF file into text
- Check if either email or phone number exists in parse text
- Iterate